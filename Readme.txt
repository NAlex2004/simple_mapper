Простой маппер объектов и выражений.

Mapper - singleton класс со статическими методами расширения и статическим экземпляром класса SimpleMapper.
SimpleMapper - можно использовать несколько экземпляров с разными маппингами, если один класс надо отображать на несколько.
В целом, для просто отображения объектов можно создать маппинги одного класса хоть на миллион в Mapper,
единственное, что конвертация выражений будет брать первый попавшийся маппинг.

ExpressionExtensions - методы расширения для конвертации деревьев выражений, использует либо Mapper, либо передавать SimpleMapper.

Пример:

	Mapper.CreateMap<ComplexDestClass, ComplexSourceClass>();
    Mapper.CreateMap<UserDest, UserSource>();

    Expression<Func<ComplexDestClass, int>> expr = d => d.User.Id;
    var mappedExpr = expr.Convert<ComplexDestClass, ComplexSourceClass, int>();
    var result = source.Select(mappedExpr.Compile());

