﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NAlex.SimpleMapper;
using System.Collections.Generic;
using System.Linq.Expressions;
using NAlex.SimpleMapper.Expressions;
using System.Linq;

namespace Tests
{
    public class SourceClass
    {
        public int field;
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        [NotMapped]
        public int NotMapped { get; set; }
    }

    public class DestinationClass
    {
        public int field;
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsShit { get; set; }
        public int NotMapped { get; set; }
        [BindedName("Value")]
        public string TheValue { get; set; }
    }

    public struct SourceStruct
    {
        public int field;
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
        [NotMapped]
        public int NotMapped { get; set; }
    }

    public struct DestinationStruct
    {
        public int field;
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsShit { get; set; }
        public int NotMapped { get; set; }
        [BindedName("Value")]
        public string TheValue { get; set; }
    }

    public class UserSource
    {
        public int Id { get; set; }
        [BindedName("Name")]
        public string UserName { get; set; }
    }

    public class UserDest
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ComplexSourceClass
    {
        public int Id { get; set; }
        public UserSource User { get; set; }
    }

    public class ComplexDestClass
    {
        public int Id { get; set; }
        public UserDest User { get; set; }
    }

    [TestClass]
    public class MappingsTests
    {
        //[TestMethod]
        //public void MappingsTestEquality()
        //{
        //    Mappings m1 = new Mappings(typeof(int), typeof(List<Type>));
        //    Mappings m2 = new Mappings(typeof(int), typeof(List<Type>));
        //    Mappings m3 = new Mappings(typeof(bool), typeof(string));

        //    Assert.AreEqual(m1, m2);
        //    Assert.AreNotEqual(m1, m3);
        //}

        [TestMethod]
        public void MapClassTestFieldsValuesAreEqual()
        {
            SourceClass source = new SourceClass()
            {
                field = 1,
                Id = 1,
                Name = "Source",
                NotMapped = 1,
                Value = "value"
            };
            Mapper.CreateMap<SourceClass, DestinationClass>();

            DestinationClass destination = source.Map<SourceClass, DestinationClass>();

            Assert.AreEqual(0, destination.field);
            Assert.AreEqual(1, destination.Id);
            Assert.AreEqual("Source", destination.Name);
            Assert.AreEqual(false, destination.IsShit);
            Assert.AreEqual(0, destination.NotMapped);
            Assert.AreEqual("value", destination.TheValue);
        }

        [TestMethod]
        public void MapStructTestFieldsValuesAreEqual()
        {
            SourceStruct source = new SourceStruct()
            {
                field = 1,
                Id = 1,
                Name = "Source",
                NotMapped = 1,
                Value = "value"
            };
            Mapper.CreateMap<SourceStruct, DestinationStruct>();

            DestinationStruct destination = (DestinationStruct)source.Map(typeof(SourceStruct), typeof(DestinationStruct));

            Assert.AreEqual(0, destination.field);
            Assert.AreEqual(1, destination.Id);
            Assert.AreEqual("Source", destination.Name);
            Assert.AreEqual(false, destination.IsShit);
            Assert.AreEqual(0, destination.NotMapped);
            Assert.AreEqual("value", destination.TheValue);
        }

        [TestMethod]
        public void MapComplexClass()
        {
            Mapper.CreateMap<UserSource, UserDest>();
            Mapper.CreateMap<ComplexSourceClass, ComplexDestClass>();

            ComplexSourceClass source = new ComplexSourceClass()
            {
                Id = 10,
                User = new UserSource()
                {
                    Id = 1,
                    UserName = "User"
                }
            };

            ComplexDestClass dest = source.Map<ComplexSourceClass, ComplexDestClass>();

            Assert.AreEqual(10, dest.Id);
            Assert.AreEqual(1, dest.User.Id);
            Assert.AreEqual("User", dest.User.Name);
        }
    }

    [TestClass]
    public class ExpressionMappingsTests
    {
        [TestMethod]
        public void MapExpressionOfSimpleClasses()
        {
            SourceClass[] source = new SourceClass[]
            {
                new SourceClass()
                {
                    Id = 1,
                    Name = "first",
                    Value = "1"
                },

                new SourceClass()
                {
                    Id = 2,
                    Name = "second",
                    Value = "2"
                },

                new SourceClass()
                {
                    Id = 3,
                    Name = "third",
                    Value = "3"
                },

                new SourceClass()
                {
                    Id = 4,
                    Name = "fourth",
                    Value = "4"
                }
            };

            Mapper.CreateMap<DestinationClass, SourceClass>();

            Expression<Func<DestinationClass, bool>> where = d => d.Name.Contains("f");
            var mappedExpr = where.Convert<DestinationClass, SourceClass, bool>();
            var result = source.Where(mappedExpr.Compile());

            Assert.AreEqual(where.ToString(), mappedExpr.ToString());
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual(1, result.First(s => s.Id == 1).Id);
            Assert.AreEqual(4, result.First(s => s.Id == 4).Id);
        }

        [TestMethod]
        public void MapExpressionOfComplexClasses()
        {
            ComplexSourceClass[] source = new ComplexSourceClass[]
            {
                new ComplexSourceClass()
                {
                    Id = 1,
                    User = new UserSource()
                    {
                        Id = 1,
                        UserName = "user 1"
                    }
                },
                new ComplexSourceClass()
                {
                    Id = 2,
                    User = new UserSource()
                    {
                        Id = 2,
                        UserName = "user 2"
                    }
                },
                new ComplexSourceClass()
                {
                    Id = 3,
                    User = new UserSource()
                    {
                        Id = 3,
                        UserName = "user 3"
                    }
                },
                new ComplexSourceClass()
                {
                    Id = 4,
                    User = new UserSource()
                    {
                        Id = 4,
                        UserName = "user 4"
                    }
                }
            };

            Mapper.CreateMap<ComplexDestClass, ComplexSourceClass>();
            Mapper.CreateMap<UserDest, UserSource>();

            Expression<Func<ComplexDestClass, int>> expr = d => d.User.Id;
            var mappedExpr = expr.Convert<ComplexDestClass, ComplexSourceClass, int>();
            var result = source.Select(mappedExpr.Compile());

            Assert.AreEqual(expr.ToString(), mappedExpr.ToString());

            Assert.AreEqual(4, result.Count());
            Assert.AreEqual(10, result.Sum(i => i));
            
        }
    }
}
