﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;

namespace NAlex.SimpleMapper.Expressions
{
    public class DTOReplaceVisitor: ExpressionVisitor
    {
        private readonly Dictionary<Type, ParameterExpression> _parameters;
        private readonly Dictionary<Type, Type> _nestedMappings;

        public DTOReplaceVisitor(Dictionary<Type, ParameterExpression> parametersMapping, Dictionary<Type, Type> nestedMappings = null)
        {
            _parameters = parametersMapping;
            _nestedMappings = nestedMappings;
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            ParameterExpression visitResult;
            if (_parameters != null && _parameters.TryGetValue(node.Type, out visitResult))
            {
                // т.к. параметры нового типа созданы в методе расширения и лямбда создается там, 
                // то тут используются они, а не 
                // Expression.Parameter(typeof(TDestination), node.Name)
                // а то Exception будет
                return visitResult;
                //return _parameters[node.Type];
            }
            return base.VisitParameter(node);
        }

        protected override Expression VisitMember(MemberExpression node)
        {
            Type declaringType = node.Member.DeclaringType;
            ParameterExpression destinationParamExpression;

            // Convert parameters
            if (node.Expression.NodeType == ExpressionType.Parameter
                    &&_parameters != null && _parameters.TryGetValue(declaringType, out destinationParamExpression) )
            {
                var param = this.VisitParameter((ParameterExpression)node.Expression);
                var result = Expression.MakeMemberAccess(param, destinationParamExpression.Type.GetMember(node.Member.Name).Single());
                return result;                
            }
            
            
            if (_nestedMappings != null && _nestedMappings.Count > 0 && node.Expression.NodeType == ExpressionType.MemberAccess)            
            {
                Type mappedNested;                

                if (_nestedMappings.TryGetValue(declaringType, out mappedNested))
                {
                    // Если тип члена еще не сменился, бегом менять! а то жопа в MakeMemberAccess
                    var expr = node.Expression.Type.Equals(declaringType) ? VisitMember((MemberExpression)node.Expression) : node.Expression;
                    var result = Expression.MakeMemberAccess(expr, mappedNested.GetMember(node.Member.Name).Single());
                    return result;
                }
            }

            return base.VisitMember(node);

        }

    }
}
