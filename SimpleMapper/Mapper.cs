﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NAlex.SimpleMapper
{
    public static class Mapper
    {
        private static SimpleMapper mapper;

        public static SimpleMapper Instance
        {
            get
            {
                if (mapper == null)
                {
                    mapper = new SimpleMapper();
                }

                return mapper;
            }
        }

        /// <summary>
        /// Maps object of sourceType to object of destinationType.
        /// Properties names must be equal or have (in one of classes) BindedNameAttribute with property name of another class.
        /// Mapping should be created before Map by CreateMap method.
        /// </summary>
        public static object Map(this object source, Type sourceType, Type destinationType)
        {
            return Instance.Map(source, sourceType, destinationType);
        }

        /// <summary>
        /// Maps TSource object to TDestination.
        /// Properties names must be equal or have (in one of classes) BindedNameAttribute with property name of another class.
        /// Mapping should be created before Map by CreateMap method.
        /// </summary>
        public static TDestination Map<TSource, TDestination>(this TSource source) where TDestination : class, new()
        {
            return Instance.Map<TSource, TDestination>(source);
        }

        public static void CreateMap<TSource, TDestination>()
        {
            Instance.CreateMap<TSource, TDestination>();
        }
    }

}
