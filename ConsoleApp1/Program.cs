﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Linq.Expressions;

namespace ConsoleApp1
{
    public class User
    {
        public int fieldId;
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public struct Shit
    {
        public string ShitName { get; set; }
    }

    public class Message
    {
        public User User { get; set; }
        public string MessageText { get; set; }
        public Shit Shit { get; set; }
        public int MID { get; set; }
    }

    public static class Extensions
    {
        public static bool IsSystemType(this Type type) => type.Assembly == typeof(object).Assembly;
        //type.Module.ScopeName == "CommonLanguageRuntimeLibrary";
    }

    class Program
    {
        

        static void Main(string[] args)
        {
            Type userType = typeof(User);
            Type messageType = typeof(Message);

            var userProps = userType.GetProperties();
            var messageProps = messageType.GetProperties();

            Console.WriteLine("------------------User---------------------");
            foreach(var prop in userProps)
            {
                Console.WriteLine($"Name = {prop.Name}\tGetType = {prop.GetType()}\tDeclaringType = {prop.DeclaringType}\tPropertyType = {prop.PropertyType}");
            }
            Console.WriteLine();
            Console.WriteLine("------------------Message---------------------");
            foreach (var prop in messageProps)
            {
                Console.WriteLine($"Name = {prop.Name}\tGetType = {prop.GetType()}\tDeclaringType = {prop.DeclaringType}\tPropertyType = {prop.PropertyType}");
                Console.WriteLine($"IsClass = {prop.PropertyType.IsClass}\tIsPrimitive = {prop.PropertyType.IsPrimitive}\tIsValueType = {prop.PropertyType.IsValueType}");
                Console.WriteLine($"IsSystemType = {prop.PropertyType.IsSystemType()}");

            }

            Expression<Func<string, bool>> expr = s => s.Contains('a');
            Console.WriteLine(expr.ToString());
            Console.ReadKey();
        }
    }
}
